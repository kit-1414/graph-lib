package com.kit1414.graphlib.mlong;

import com.kit1414.graphlib.model.mlong.EdgeLong;
import com.kit1414.graphlib.model.mlong.GraphLong;
import com.kit1414.graphlib.model.mlong.VertexLong;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * Class presents simple test case on Square graph
 */
public class SquarePathTest extends GraphLongTestBase {

    // create square Graph
    VertexLong v1 = buildVertex(1L);
    VertexLong v2 = buildVertex(2L);
    VertexLong v3 = buildVertex(3L);
    VertexLong v4 = buildVertex(4L);

    EdgeLong e1 = buildEdge(v1, v2);
    EdgeLong e2 = buildEdge(v2, v3);
    EdgeLong e3 = buildEdge(v3, v4);
    EdgeLong e4 = buildEdge(v4, v1);

    List<VertexLong> vList = Arrays.asList(v1, v2, v3, v4);
    List<EdgeLong> eList = Arrays.asList(e1, e2, e3, e4);

    GraphLong graph = buildGraph(vList, eList);

    // define start-end vertexes
    VertexLong startVertex = v1;
    VertexLong endVertex = v3;

    // define expected all possible pathes for defined start-end vertexes
    List<List<EdgeLong>> possiblePathList = Arrays.asList(
            Arrays.asList(e1, e2),
            Arrays.asList(e4, e3)
    );


    @Test
    public void onePathTest() {
        doOnePathTest(startVertex, endVertex, graph, possiblePathList);
    }

    @Test
    public void allPathTest() {
        doAllPathTest(startVertex, endVertex, graph, possiblePathList);
    }
}