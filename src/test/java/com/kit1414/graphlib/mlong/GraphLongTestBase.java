package com.kit1414.graphlib.mlong;

import com.kit1414.graphlib.model.base.EdgeDirection;
import com.kit1414.graphlib.model.mlong.EdgeLong;
import com.kit1414.graphlib.model.mlong.GraphLong;
import com.kit1414.graphlib.model.mlong.GraphLongFactory;
import com.kit1414.graphlib.model.mlong.VertexLong;
import lombok.extern.slf4j.Slf4j;

import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Class presents base operation for test cases
 */
@Slf4j
public class GraphLongTestBase {
    public static long DEF_WEIGHT = 1;
    public static EdgeDirection DEF_DIRECTION = EdgeDirection.BI_DIRECTION;

    protected EdgeLong buildEdge(VertexLong v1, VertexLong v2) {
        return buildEdge(v1.getVertexId(), v2.getVertexId());
    }

    protected EdgeLong buildEdge(Long v1Id, Long v2Id) {
        return buildEdge(v1Id, v2Id, DEF_WEIGHT, DEF_DIRECTION);
    }


    protected EdgeLong buildEdge(VertexLong v1, VertexLong v2, EdgeDirection direction) {
        return new EdgeLong(nextId(), DEF_WEIGHT, direction, v1.getVertexId(), v2.getVertexId());
    }

    protected EdgeLong buildEdge(Long v1Id, Long v2Id, long weight, EdgeDirection direction) {
        return new EdgeLong(nextId(), weight, direction, v1Id, v2Id);
    }


    protected boolean isSameAndNonEmpty(List<EdgeLong> l1, List<EdgeLong> l2) {
        if (l1.isEmpty() || l1.size() != l2.size()) {
            return false;
        }
        Iterator<EdgeLong> li1 = l1.iterator();
        Iterator<EdgeLong> li2 = l1.iterator();
        while (li1.hasNext()) {
            EdgeLong e1 = li1.next();
            EdgeLong e2 = li2.next();
            if (e1.getEdgeId().longValue() != e2.getEdgeId()) {
                return false;
            }
        }
        return true;
    }

    protected boolean isOneOf(List<EdgeLong> path, List<List<EdgeLong>> possiblePathes) {
        for (List<EdgeLong> checkPath : possiblePathes) {
            if (isSameAndNonEmpty(path, checkPath)) {
                return true;
            }
        }
        return false;
    }

    protected boolean isSamePathSet(List<List<EdgeLong>> actualPathSet, List<List<EdgeLong>> expectedPathSet) {
        if (expectedPathSet.isEmpty() || expectedPathSet.size() != actualPathSet.size()) {
            return false;
        }

        for (List<EdgeLong> checkPath : actualPathSet) {
            if (!isOneOf(checkPath, expectedPathSet)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Test check that founded path is one of expected
     */
    protected void doOnePathTest(VertexLong startVertex, VertexLong endVertex, GraphLong graph, List<List<EdgeLong>> possiblePathList) {
        log.debug("getPath( {}, {} ):", startVertex, endVertex);
        List<EdgeLong> resultPath = graph.getPath(startVertex.getVertexId(), endVertex.getVertexId());
        resultPath.forEach(it -> log.debug(it.toString()));

        boolean pathFound = isOneOf(resultPath, possiblePathList);
        assertTrue("one oof 2 valid pathes must be found", pathFound);
    }

    /**
     * Test check that all expected path founded
     */
    protected void doAllPathTest(VertexLong startVertex, VertexLong endVertex, GraphLong graph, List<List<EdgeLong>> possiblePathList) {
        List<List<EdgeLong>> resultPathList = graph.getAllPath(startVertex.getVertexId(), endVertex.getVertexId());
        log.debug("getAllPath( {}, {} ):", startVertex, endVertex);
        int counter = 1;
        log.debug("=== There are {} pathes found ====", resultPathList.size());
        for (List<EdgeLong> path : resultPathList) {
            log.debug("=== Path {} of {} ====", counter, resultPathList.size());
            path.forEach(it -> log.debug(it.toString()));
            counter++;
        }

        boolean allPathFound = isSamePathSet(resultPathList, possiblePathList);
        assertTrue("all valid path must be found", allPathFound);
    }

    protected VertexLong buildVertex(Long id) {
        return new VertexLong(id);
    }

    protected Long nextId() {
        return GraphLongFactory.getNextId();
    }

    protected GraphLong buildGraph() {
        return GraphLongFactory.buildThreadSafeGraph();
    }

    protected GraphLong buildGraph(List<VertexLong> vList, List<EdgeLong> eList) {
        GraphLong graph = buildGraph();
        // build square and find path for 2 opposite vertexes
        graph.addVertexes(vList);
        graph.addEdges(eList);

        log.debug("getEdges():");
        graph.getEdges().forEach(it -> log.debug(it.toString()));

        log.debug("getVertexes():");
        graph.getVertexes().forEach(it -> log.debug(it.toString()));

        return graph;
    }
}
