package com.kit1414.graphlib.mlong;

import com.kit1414.graphlib.model.base.EdgeDirection;
import com.kit1414.graphlib.model.mlong.EdgeLong;
import com.kit1414.graphlib.model.mlong.GraphLong;
import com.kit1414.graphlib.model.mlong.VertexLong;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Class presents test case on Twice-Square graph
 */
public class TwiceSquareComplexPathTest extends GraphLongTestBase {
    // create the Graph
    VertexLong v1 = buildVertex(1L);
    VertexLong v2 = buildVertex(2L);
    VertexLong v3 = buildVertex(3L);
    VertexLong v4 = buildVertex(4L);
    VertexLong v5 = buildVertex(5L);
    VertexLong v6 = buildVertex(6L);
    VertexLong v7 = buildVertex(7L);

    EdgeLong e1 = buildEdge(v1, v2);
    EdgeLong e2 = buildEdge(v2, v3);
    EdgeLong e3 = buildEdge(v3, v4);
    EdgeLong e4 = buildEdge(v4, v1);

    EdgeLong e5 = buildEdge(v3, v5);
    EdgeLong e6 = buildEdge(v5, v6);
    EdgeLong e7 = buildEdge(v6, v7);
    EdgeLong e8 = buildEdge(v7, v3, EdgeDirection.ONE_DIRECTION);
    EdgeLong e9 = buildEdge(v3, v6, EdgeDirection.ONE_DIRECTION);

    List<VertexLong> vList = Arrays.asList(v1, v2, v3, v4, v5, v6, v7);
    List<EdgeLong> eList = Arrays.asList(e1, e2, e3, e4, e5, e6, e7, e8, e9);

    GraphLong graph = buildGraph(vList, eList);

    // first test data set
    // define start-end vertexes
    VertexLong startVertex1 = v1;
    VertexLong endVertex1 = v6;

    // define expected all possible pathes for defined start-end vertexes
    List<List<EdgeLong>> possiblePathList1 = Arrays.asList(
            Arrays.asList(e1, e2, e5, e6),
            Arrays.asList(e3, e4, e5, e6),
            Arrays.asList(e1, e2, e9),
            Arrays.asList(e3, e4, e9)
    );

    // second test data set
    VertexLong startVertex2 = v5;
    VertexLong endVertex2 = v6;

    List<List<EdgeLong>> possiblePathList2 = Arrays.asList(
            Collections.singletonList(e6),
            Arrays.asList(e5, e9)
    );

    // third test data set
    VertexLong startVertex3 = v4;
    VertexLong endVertex3 = v7;

    List<List<EdgeLong>> possiblePathList3 = Arrays.asList(
            Arrays.asList(e3, e9, e7),
            Arrays.asList(e3, e5, e6, e7),
            Arrays.asList(e4, e1, e2, e9, e7),
            Arrays.asList(e4, e1, e2, e5, e6, e7)
    );

    @Test
    public void onePathTest1() {
        doOnePathTest(startVertex1, endVertex1, graph, possiblePathList1);
    }

    @Test
    public void allPathTest1() {
        doAllPathTest(startVertex1, endVertex1, graph, possiblePathList1);
    }

    @Test
    public void onePathTest2() {
        doOnePathTest(startVertex2, endVertex2, graph, possiblePathList2);
    }

    @Test
    public void allPathTest2() {
        doAllPathTest(startVertex2, endVertex2, graph, possiblePathList2);
    }

    @Test
    public void onePathTest3() {
        doOnePathTest(startVertex3, endVertex3, graph, possiblePathList3);
    }

    @Test
    public void allPathTest3() {
        doAllPathTest(startVertex3, endVertex3, graph, possiblePathList3);
    }
}
