package com.kit1414.graphlib.mlong;

import com.kit1414.graphlib.exception.GraphException;
import com.kit1414.graphlib.model.base.EdgeDirection;
import com.kit1414.graphlib.model.mlong.EdgeLong;
import com.kit1414.graphlib.model.mlong.GraphLong;
import com.kit1414.graphlib.model.mlong.VertexLong;
import org.junit.Test;

/**
 * Class presents negative test cases.
 */
public class NegativeTest extends GraphLongTestBase {

    @Test(expected = GraphException.class)
    public void addVertexNullTest() {
        buildGraph().addVertex(null);
    }

    @Test(expected = GraphException.class)
    public void addVertexTwiceIdTest() {
        GraphLong g1 = buildGraph();
        g1.addVertex(buildVertex(1L));
        g1.addVertex(buildVertex(1L));
    }

    @Test(expected = GraphException.class)
    public void addEdgeNullTest() {
        buildGraph().addEdge(null);
    }

    @Test(expected = GraphException.class)
    public void addEdgeTwiceIdTest() {
        VertexLong v1 = buildVertex(1L);
        VertexLong v2 = buildVertex(2L);
        VertexLong v3 = buildVertex(3L);

        GraphLong g1 = buildGraph();
        g1.addVertex(v1);
        g1.addVertex(v2);
        g1.addVertex(v3);

        long id = 5L;
        long weight = 1L;
        EdgeDirection d = EdgeDirection.BI_DIRECTION;
        // ID can't be same
        EdgeLong e1 = new EdgeLong(id, weight, d, v1.getVertexId(), v2.getVertexId());
        EdgeLong e2 = new EdgeLong(id, weight, d, v1.getVertexId(), v3.getVertexId());

        g1.addEdge(e1);
        g1.addEdge(e2);
    }

    @Test(expected = GraphException.class)
    public void addEdgeTwiceVertexesTest() {
        VertexLong v1 = buildVertex(1L);
        VertexLong v2 = buildVertex(2L);
        VertexLong v3 = buildVertex(3L);

        GraphLong g1 = buildGraph();
        g1.addVertex(v1);
        g1.addVertex(v2);
        g1.addVertex(v3);

        long weight = 1L;
        EdgeDirection d = EdgeDirection.BI_DIRECTION;
        EdgeLong e1 = new EdgeLong(6L, weight, d, v1.getVertexId(), v2.getVertexId());
        EdgeLong e2 = new EdgeLong(7L, weight, d, v1.getVertexId(), v3.getVertexId());

        // duplicate vertex pair
        EdgeLong e3 = new EdgeLong(8L, weight, d, v1.getVertexId(), v3.getVertexId());

        g1.addEdge(e1);
        g1.addEdge(e2);

        g1.addEdge(e3); // error operation
    }

    @Test(expected = GraphException.class)
    public void addEdgeNullDirectionTest() {
        VertexLong v1 = buildVertex(1L);
        VertexLong v2 = buildVertex(2L);

        GraphLong g1 = buildGraph();
        g1.addVertex(v1);
        g1.addVertex(v2);

        long id = 5L;
        long weight = 1L;
        EdgeDirection d = null; // direction can't be null
        EdgeLong e1 = new EdgeLong(id, weight, d, v1.getVertexId(), v2.getVertexId());

        g1.addEdge(e1);
    }

    @Test(expected = GraphException.class)
    public void findPathSameVertexes() {
        VertexLong v1 = buildVertex(1L);
        VertexLong v2 = buildVertex(2L);

        GraphLong g1 = buildGraph();
        g1.addVertex(v1);
        g1.addVertex(v2);

        long id = 5L;
        long weight = 1L;
        EdgeDirection d = EdgeDirection.BI_DIRECTION;
        EdgeLong e1 = new EdgeLong(id, weight, d, v1.getVertexId(), v2.getVertexId());
        g1.addEdge(e1);
        g1.getPath(v1.getVertexId(), v1.getVertexId());
    }

    @Test(expected = GraphException.class)
    public void findPathWrongVertexeA() {
        VertexLong v1 = buildVertex(1L);
        VertexLong v2 = buildVertex(2L);

        GraphLong g1 = buildGraph();
        g1.addVertex(v1);
        g1.addVertex(v2);

        long id = 5L;
        long weight = 1L;
        EdgeDirection d = EdgeDirection.BI_DIRECTION;
        EdgeLong e1 = new EdgeLong(id, weight, d, v1.getVertexId(), v2.getVertexId());
        g1.addEdge(e1);
        g1.getPath(77L, v1.getVertexId());
    }

    @Test(expected = GraphException.class)
    public void findPathWrongVertexeB() {
        VertexLong v1 = buildVertex(1L);
        VertexLong v2 = buildVertex(2L);

        GraphLong g1 = buildGraph();
        g1.addVertex(v1);
        g1.addVertex(v2);

        long id = 5L;
        long weight = 1L;
        EdgeDirection d = EdgeDirection.BI_DIRECTION;
        EdgeLong e1 = new EdgeLong(id, weight, d, v1.getVertexId(), v2.getVertexId());
        g1.addEdge(e1);
        g1.getPath(v1.getVertexId(), 77L);
    }

    @Test(expected = GraphException.class)
    public void findPathNullVertexeA() {
        VertexLong v1 = buildVertex(1L);
        VertexLong v2 = buildVertex(2L);

        GraphLong g1 = buildGraph();
        g1.addVertex(v1);
        g1.addVertex(v2);

        long id = 5L;
        long weight = 1L;
        EdgeDirection d = EdgeDirection.BI_DIRECTION;
        EdgeLong e1 = new EdgeLong(id, weight, d, v1.getVertexId(), v2.getVertexId());
        g1.addEdge(e1);
        g1.getPath(null, v1.getVertexId());
    }

    @Test(expected = GraphException.class)
    public void findPathNullVertexeB() {
        VertexLong v1 = buildVertex(1L);
        VertexLong v2 = buildVertex(2L);

        GraphLong g1 = buildGraph();
        g1.addVertex(v1);
        g1.addVertex(v2);

        long id = 5L;
        long weight = 1L;
        EdgeDirection d = EdgeDirection.BI_DIRECTION;
        EdgeLong e1 = new EdgeLong(id, weight, d, v1.getVertexId(), v2.getVertexId());
        g1.addEdge(e1);
        g1.getPath(v1.getVertexId(), null);
    }
}
