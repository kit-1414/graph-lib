package com.kit1414.graphlib.model.base;

import com.kit1414.graphlib.exception.GraphException;

import java.util.*;

/**
 * Help class. Presents Path in graph.
 **/
public class GraphPath<V extends Vertex<T>, E extends Edge<V, T>, T> {
    // stores all Vertexes of path
    private final Set<T> vertexIds = new HashSet<>();

    // stores all Vertexes as sequence from start to end (stack)
    private final Deque<T> vertexIdQueue = new LinkedList<>();

    // stores all Edges as sequence from start to end (stack)
    private final Deque<E> edges = new LinkedList<>();

    public GraphPath(T startVertexId) {
        vertexIds.add(startVertexId);
        vertexIdQueue.push(startVertexId);
    }

    public List<T> getPathAsVertexIds() {
        return new ArrayList<>(vertexIdQueue);
    }

    public List<E> getPathAsEdges() {
        return new ArrayList<>(edges);
    }

    public int getVertexNumber() {
        return vertexIds.size();
    }

    public T getLastVertexId() {
        return vertexIdQueue.getLast();
    }

    public void removeLast() {
        if (edges.size() > 0) {
            E e = edges.removeLast();
            T lastVId = vertexIdQueue.removeLast();
            vertexIds.remove(lastVId);
        }
    }

    public boolean canAddEdge(E e) {
        return checkCanAddEdge(e) == null;
    }

    // validate Edge before add to path
    private String checkCanAddEdge(E e) {
        T lastVId = getLastVertexId();
        T aId = e.getVertexAId();
        T bId = e.getVertexBId();
        T newVId = getAnotherVertex(lastVId, e);

        if (!(lastVId.equals(aId) || lastVId.equals(bId))) {
            return "Can't add edge - no connected vertex";
        }

        if (vertexIds.contains(newVId)) {
            return "Can't add edge - duplicate vertex";
        }

        if (e.getDirection() != EdgeDirection.BI_DIRECTION && newVId.equals(aId)) {
            // A -> B - ok, B -> A (A - new Id) - Error
            return "Can't add edge -wrong direction";
        }
        return null;
    }

    public void add(E e) {
        String error = checkCanAddEdge(e);
        if (error != null) {
            throw new GraphException(error);
        }
        T lastVId = getLastVertexId();
        T newVId = getAnotherVertex(lastVId, e);

        vertexIds.add(newVId);
        vertexIdQueue.addLast(newVId);
        edges.addLast(e);
    }

    private T getAnotherVertex(T vId, E e) {
        T lastVId = getLastVertexId();
        T aId = e.getVertexAId();
        T bId = e.getVertexBId();
        return lastVId.equals(aId) ? bId : aId;
    }

    @Override
    public String toString() {
        return "GraphPath{" +
                " vertexIdQueue=" + vertexIdQueue +
                ", edges=" + edges +
                '}';
    }
}
