package com.kit1414.graphlib.model.base.algotithm;

import com.kit1414.graphlib.model.base.Edge;
import com.kit1414.graphlib.model.base.GraphPath;
import com.kit1414.graphlib.model.base.Vertex;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * Class present visitor for all path finder
 * <p>
 * Graph scan for all path between vertexes
 */
@Getter
public class GraphAllPathesFinder<V extends Vertex<T>, E extends Edge<V, T>, T> implements GraphPathVisitor<V, E, T> {
    private final T lastVertexId;
    private List<List<E>> result = new ArrayList<>();

    public GraphAllPathesFinder(T lastVertexId) {
        this.lastVertexId = lastVertexId;
    }

    /**
     * Accumulate all pathes with second end == lastVertexId
     */
    @Override
    public boolean onEdgeAdded(GraphPath<V, E, T> path) {
        if (lastVertexId.equals(path.getLastVertexId())) {
            result.add(path.getPathAsEdges());
        }
        return true;
    }
}
