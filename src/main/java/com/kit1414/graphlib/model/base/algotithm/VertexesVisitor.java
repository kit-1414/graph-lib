package com.kit1414.graphlib.model.base.algotithm;

import com.kit1414.graphlib.model.base.Vertex;

/**
 * Interface for user-defined method for vertexes scan
 */
public interface VertexesVisitor<V extends Vertex<T>, T> {
    void onVertex(V v);
}
