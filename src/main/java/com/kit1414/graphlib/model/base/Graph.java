package com.kit1414.graphlib.model.base;

import com.kit1414.graphlib.model.base.algotithm.VertexesVisitor;

import java.util.List;

/**
 * Presents Graph interface
 *
 * @param <V> - Vertex class type
 * @param <E> - Edge class type
 * @param <T> - ID class type. Must be with valid equals(0 && hashCode()
 */
public interface Graph<V extends Vertex<T>, E extends Edge<V, T>, T> {

    void addVertex(V v);

    void addVertexes(List<V> vList);

    void addEdge(E e);

    void addEdges(List<E> eList);

    List<V> getVertexes();

    List<E> getEdges();

    /**
     * Method return one (any) path from vertex with id=v1Id to vertex with id =v2Id
     * Any Vertex happen one in path.
     *
     * @param v1Id - start vertex Id
     * @param v2Id - end vertex Id
     * @return Edges list as path
     */
    List<E> getPath(T v1Id, T v2Id);

    /**
     * Method return all possible path from vertex with id=v1Id to vertex with id =v2Id
     * Any Vertex happen one in path.
     *
     * @param v1Id - start vertex Id
     * @param v2Id - end vertex Id
     * @return List of Edges list as path list
     */
    List<List<E>> getAllPath(T v1Id, T v2Id);

    /**
     * Method apply user defined method of VertexesVisitor::onVertex(V v) to all vertexes of Graph
     *
     * @param vt - instance of VertexesVisitor
     */
    void traverseVertexes(VertexesVisitor<V, T> vt);

}