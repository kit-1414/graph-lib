package com.kit1414.graphlib.model.base;

import com.kit1414.graphlib.exception.GraphException;
import com.kit1414.graphlib.model.base.algotithm.GraphAllPathesFinder;
import com.kit1414.graphlib.model.base.algotithm.GraphAnyPathFinder;
import com.kit1414.graphlib.model.base.algotithm.GraphPathVisitor;
import com.kit1414.graphlib.model.base.algotithm.VertexesVisitor;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Generic implementation of Graph
 *
 * @param <V> - Vertex class type
 * @param <E> - Edge class type
 * @param <T> - ID class type. Must be with valid equals(0 && hashCode()
 */
public class GraphBase<V extends Vertex<T>, E extends Edge<V, T>, T> implements Graph<V, E, T> {
    // edges collection: edgeId -> Edge
    private final Map<T, E> edges = new HashMap<>();

    //vertexes collection: vertexId -> Vertex
    private final Map<T, V> vertexes = new HashMap<>();

    //mapping: VertexId -> all edges with Vertex on any end
    private final Map<T, List<E>> edgesByVertex = new HashMap<>();


    @Override
    public List<V> getVertexes() {
        return new ArrayList<>(vertexes.values());
    }

    @Override
    public List<E> getEdges() {
        return new ArrayList<>(edges.values());
    }

    /**
     * See interface method description
     * {@link com.kit1414.graphlib.model.base.Graph#getPath(T, T)}
     */
    @Override
    public List<E> getPath(T v1Id, T v2Id) {
        validatePathVertexes(v1Id, v2Id);
        GraphAnyPathFinder<V, E, T> graphWalker = new GraphAnyPathFinder<>(v2Id);
        walkThrowGraph(v1Id, graphWalker);
        return graphWalker.getResult();
    }

    /**
     * See interface method description
     * {@link com.kit1414.graphlib.model.base.Graph#getAllPath(T, T)}
     */
    @Override
    public List<List<E>> getAllPath(T v1Id, T v2Id) {
        validatePathVertexes(v1Id, v2Id);
        GraphAllPathesFinder<V, E, T> graphWalker = new GraphAllPathesFinder<>(v2Id);
        walkThrowGraph(v1Id, graphWalker);
        return graphWalker.getResult();
    }

    private void validatePathVertexes(T v1Id, T v2Id) {
        if (v1Id == null || v2Id == null) {
            throwError("Can't build path, vertex(es) are null");
        }

        if (!vertexes.containsKey(v1Id) || !vertexes.containsKey(v2Id)) {
            throwError("Can't build path, vertex(es) is not in graph");
        }

        if (v1Id.equals(v2Id)) {
            throwError("Can't build path, vertex(es) are same");
        }
    }

    /**
     * Method walk throw graph by all possible path from start vertex
     *
     * @param startVertexId - start vertex ID
     * @param graphWalker   - Path visitor class
     */
    public void walkThrowGraph(T startVertexId, GraphPathVisitor<V, E, T> graphWalker) {
        GraphPath<V, E, T> path = new GraphPath<>(startVertexId);
        LinkedList<List<E>> edgesScanStack = new LinkedList<>();
        addNewAgesToScan(path.getLastVertexId(), edgesScanStack);
        while (!edgesScanStack.isEmpty()) {
            List<E> edgesToScan = edgesScanStack.getLast();
            if (edgesToScan.isEmpty()) {
                removeLastOnStackIfEmpty(edgesScanStack);
                path.removeLast();
            } else {
                E edge = edgesToScan.remove(0);
                if (path.canAddEdge(edge)) {
                    path.add(edge);
                    if (!graphWalker.onEdgeAdded(path)) {
                        // stop scan
                        return;
                    }
                    addNewAgesToScan(path.getLastVertexId(), edgesScanStack);
                }
            }
        }
    }

    private void removeLastOnStackIfEmpty(LinkedList<List<E>> searchStack) {
        if (searchStack.getLast().isEmpty()) {
            searchStack.removeLast();
        }
    }

    private void addNewAgesToScan(T vId, LinkedList<List<E>> edgesScanStack) {
        List<E> possibleEdges = new ArrayList<>(this.edgesByVertex.get(vId));
        if (!possibleEdges.isEmpty()) {
            edgesScanStack.add(possibleEdges);
        }
    }

    /**
     * See interface method description
     * {@link com.kit1414.graphlib.model.base.Graph#traverseVertexes(VertexesVisitor)} )}
     */
    @Override
    public void traverseVertexes(VertexesVisitor<V, T> vt) {
        vertexes.values().forEach(vt::onVertex);
    }


    @Override
    public void addVertexes(List<V> vList) {
        vList.forEach(this::addVertex);
    }

    @Override
    public void addVertex(V v) {
        T vId = validateNewVertex(v);
        vertexes.put(vId, v);
        edgesByVertex.put(vId, new ArrayList<>());
    }

    private T validateNewVertex(V v) {
        if (v == null) {
            throwError("Vertex can't be null");
        }

        T vId = v.getVertexId();
        if (vId == null) {
            throwError("Vertex Id can't be null");
        }
        if (vertexes.containsKey(vId)) {
            throwError("Vertex with the ID already exists");
        }

        return vId;
    }

    private T validateVertexIdForNewEdge(T vId) {
        if (vId == null) {
            throwError("Vertex Id can't be null");
        }
        if (!vertexes.containsKey(vId)) {
            throwError("Vertex Id not found");
        }
        return vId;
    }

    @Override
    public void addEdge(E e) {
        validateNewEdge(e);
        edges.put(e.getEdgeId(), e);
        edgesByVertex.get(e.getVertexAId()).add(e);
        edgesByVertex.get(e.getVertexBId()).add(e);
    }

    @Override
    public void addEdges(List<E> eList) {
        eList.forEach(this::addEdge);
    }

    private void validateNewEdge(E e) {
        if (e == null) {
            throwError("Edge can't be null");
        }
        T eId = e.getEdgeId();
        if (eId == null) {
            throwError("Edge Id can't be null");
        }

        if (e.getDirection() == null) {
            throwError("Edge Direction can't be null");
        }

        if (e.getWeight() == null) {
            throwError("Edge Weight can't be null");
        }

        if (edges.containsKey(eId)) {
            throwError("Edge Id can't be duplicated");
        }

        // check Edge with save vertexes already exists:
        T vaId = validateVertexIdForNewEdge(e.getVertexAId());
        T vbId = validateVertexIdForNewEdge(e.getVertexBId());
        List<E> edgesByA = edgesByVertex.computeIfAbsent(vaId, (it) -> new ArrayList<>());
        List<E> edgesByB = edgesByVertex.computeIfAbsent(vbId, (it) -> new ArrayList<>());

        Set<T> edgesIdsByA = edgesByA.stream().map(E::getEdgeId).collect(Collectors.toSet());
        Set<T> edgesIdsByB = edgesByB.stream().map(E::getEdgeId).collect(Collectors.toSet());

        // check intersection
        edgesIdsByA.retainAll(edgesIdsByB);
        if (edgesIdsByA.size() > 0) {
            // edgesIdsByA.size() must be == 1
            T id = edgesIdsByA.iterator().next();
            E edge = edges.get(id);

            String errorMsg = String.format("Edge with same vertexes ([%s],[%s]) already exists: %s",
                    e.getVertexAId().toString(),
                    e.getVertexBId().toString(),
                    edge.toString()
            );
            throw new GraphException(errorMsg);
        }
    }

    private void throwError(String msg) {
        throw new GraphException(msg);
    }
}
