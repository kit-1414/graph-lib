package com.kit1414.graphlib.model.base;

/**
 * Enum for Edge direction type.
 */
public enum EdgeDirection {
    BI_DIRECTION, // means a -> b and b -> a directions both
    ONE_DIRECTION // means a -> b direction only
}
