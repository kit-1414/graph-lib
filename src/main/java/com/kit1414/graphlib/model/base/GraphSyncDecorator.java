package com.kit1414.graphlib.model.base;

import com.kit1414.graphlib.model.base.algotithm.VertexesVisitor;

import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Class presents Thread-Safe decorator for Graph.
 * <p>
 * It based on ReentrantReadWriteLock.
 */
public class GraphSyncDecorator<V extends Vertex<T>, E extends Edge<V, T>, T> implements Graph<V, E, T> {
    private final Graph<V, E, T> target;
    private final ReentrantReadWriteLock rwLock;

    public GraphSyncDecorator(Graph<V, E, T> target) {
        this.target = target;
        this.rwLock = new ReentrantReadWriteLock();
    }

    @Override
    public List<V> getVertexes() {
        ReentrantReadWriteLock.ReadLock rl = rwLock.readLock();
        rl.lock();
        try {
            return target.getVertexes();
        } finally {
            rl.unlock();
        }
    }

    @Override
    public List<E> getEdges() {
        ReentrantReadWriteLock.ReadLock rl = rwLock.readLock();
        rl.lock();
        try {
            return target.getEdges();
        } finally {
            rl.unlock();
        }
    }

    @Override
    public List<E> getPath(T v1Id, T v2Id) {
        ReentrantReadWriteLock.ReadLock rl = rwLock.readLock();
        rl.lock();
        try {
            return target.getPath(v1Id, v2Id);
        } finally {
            rl.unlock();
        }
    }

    @Override
    public List<List<E>> getAllPath(T v1Id, T v2Id) {
        ReentrantReadWriteLock.ReadLock rl = rwLock.readLock();
        rl.lock();
        try {
            return target.getAllPath(v1Id, v2Id);
        } finally {
            rl.unlock();
        }
    }

    @Override
    public void traverseVertexes(VertexesVisitor<V, T> vt) {
        ReentrantReadWriteLock.ReadLock rl = rwLock.readLock();
        rl.lock();
        try {
            target.traverseVertexes(vt);
        } finally {
            rl.unlock();
        }
    }

    @Override
    public void addVertexes(List<V> vList) {
        ReentrantReadWriteLock.WriteLock wl = rwLock.writeLock();
        wl.lock();
        try {
            target.addVertexes(vList);
        } finally {
            wl.unlock();
        }
    }

    @Override
    public void addVertex(V v) {
        ReentrantReadWriteLock.WriteLock wl = rwLock.writeLock();
        wl.lock();
        try {
            target.addVertex(v);
        } finally {
            wl.unlock();
        }
    }

    @Override
    public void addEdge(E e) {
        ReentrantReadWriteLock.WriteLock wl = rwLock.writeLock();
        wl.lock();
        try {
            target.addEdge(e);
        } finally {
            wl.unlock();
        }
    }

    @Override
    public void addEdges(List<E> eList) {
        ReentrantReadWriteLock.WriteLock wl = rwLock.writeLock();
        wl.lock();
        try {
            target.addEdges(eList);
        } finally {
            wl.unlock();
        }
    }
}
