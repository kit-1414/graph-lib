package com.kit1414.graphlib.model.base;

/**
 * Interface present Edge.
 * <p>
 * All methods must return not null const values.
 *
 * @param <T> - must be object with valid pair equals() && hashCode()*
 */
public interface Edge<V extends Vertex<T>, T> {
    /**
     * The method must returns const value after instance created.
     *
     * @return vertex ID. It must not be null.
     */
    T getEdgeId();

    /**
     * The method must return const value after instance created.
     *
     * @return vertex A ID. It must not be null.
     */
    T getVertexAId();

    /**
     * The method must return const value after instance created.
     *
     * @return vertex B ID. It must not be null.
     */
    T getVertexBId();

    /**
     * The method must return const value after instance created.
     *
     * @return edge direction. It must not be null.
     */
    EdgeDirection getDirection();

    /**
     * The method must return const value after instance created.
     *
     * @return edge weight. It must not be null.
     */
    Long getWeight();
}
