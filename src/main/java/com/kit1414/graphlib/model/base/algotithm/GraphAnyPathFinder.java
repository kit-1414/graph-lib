package com.kit1414.graphlib.model.base.algotithm;

import com.kit1414.graphlib.model.base.Edge;
import com.kit1414.graphlib.model.base.GraphPath;
import com.kit1414.graphlib.model.base.Vertex;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * Class present visitor for one path finder
 * <p>
 * Graph scan stop after 1st any path founded.
 */
@Getter
public class GraphAnyPathFinder<V extends Vertex<T>, E extends Edge<V, T>, T> implements GraphPathVisitor<V, E, T> {
    private final T lastVertexId;
    private List<E> result = new ArrayList<>();

    public GraphAnyPathFinder(T lastVertexId) {
        this.lastVertexId = lastVertexId;
    }

    /**
     * Method looking for first path to end Vertex and stop Graph scan.
     */
    @Override
    public boolean onEdgeAdded(GraphPath<V, E, T> path) {
        if (lastVertexId.equals(path.getLastVertexId())) {
            result.addAll(path.getPathAsEdges());
            // target vertex founded. Stop walking
            return false;
        }
        // continue graph walking
        return true;
    }
}
