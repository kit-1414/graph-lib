package com.kit1414.graphlib.model.base.algotithm;

import com.kit1414.graphlib.model.base.Edge;
import com.kit1414.graphlib.model.base.GraphPath;
import com.kit1414.graphlib.model.base.Vertex;

/**
 * Interface for Graph Path scanner  - GOF Visitor
 */
public interface GraphPathVisitor<V extends Vertex<T>, E extends Edge<V, T>, T> {
    /**
     * Method calls on any Path in graph
     *
     * @param path - path with new edge added
     * @return false for stop graph path scan, another - true
     */
    boolean onEdgeAdded(GraphPath<V, E, T> path);
}
