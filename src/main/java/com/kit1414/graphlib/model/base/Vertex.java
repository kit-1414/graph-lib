package com.kit1414.graphlib.model.base;

/**
 * Interface present Vertex.
 * <p>
 * All methods must return not null const values.
 *
 * @param <T> - must be object with valid pair equals() && hashCode()*
 */
public interface Vertex<T> {
    /**
     * The method must return const value after instance created.
     *
     * @return vertex ID. Must not be null
     */
    T getVertexId();
}
