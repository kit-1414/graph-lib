package com.kit1414.graphlib.model.mlong;

import com.kit1414.graphlib.model.base.GraphBase;

/**
 * Graph implementation With Long as ID
 */
public class GraphBaseLong extends GraphBase<VertexLong, EdgeLong, Long> implements GraphLong {
}
