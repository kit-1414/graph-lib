package com.kit1414.graphlib.model.mlong;

import com.kit1414.graphlib.model.base.Vertex;
import lombok.Getter;

/**
 * Simple Vertex implementation With Long as Vertex ID
 */
@Getter
public class VertexLong implements Vertex<Long> {
    private final Long vertexId;

    public VertexLong(long id) {
        this.vertexId = id;
    }

    @Override
    public String toString() {
        return "VertexLong{" +
                "vId=" + vertexId +
                '}';
    }
}
