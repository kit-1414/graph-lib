package com.kit1414.graphlib.model.mlong;

import com.kit1414.graphlib.model.base.Graph;
import com.kit1414.graphlib.model.base.GraphSyncDecorator;

/**
 * Graph Thread Safe decorator implementation With Long as ID
 */
public class GraphLongSyncDecorator extends GraphSyncDecorator<VertexLong, EdgeLong, Long> implements GraphLong {
    public GraphLongSyncDecorator(Graph<VertexLong, EdgeLong, Long> target) {
        super(target);
    }
}
