package com.kit1414.graphlib.model.mlong;

import com.kit1414.graphlib.model.base.Edge;
import com.kit1414.graphlib.model.base.EdgeDirection;
import lombok.Getter;

/**
 * Simple Edge implementation With Long as Edge ID
 */
@Getter
public class EdgeLong implements Edge<VertexLong, Long> {
    private final Long edgeId;
    private final Long weight;
    private final Long vertexAId;
    private final Long vertexBId;
    private final EdgeDirection direction;

    public EdgeLong(long id, long weight, EdgeDirection direction, long vertexAid, long vertexBId) {
        this.edgeId = id;
        this.weight = weight;
        this.direction = direction;
        this.vertexAId = vertexAid;
        this.vertexBId = vertexBId;
    }

    @Override
    public String toString() {
        return "EdgeLong{" +
                " eId=" + edgeId +
                ", vAId=" + vertexAId +
                ", vBId=" + vertexBId +
                ", weight=" + weight +
                ", direction=" + direction +
                '}';
    }
}
