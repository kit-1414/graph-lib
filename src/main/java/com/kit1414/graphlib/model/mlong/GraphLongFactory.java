package com.kit1414.graphlib.model.mlong;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Object instaces factory for Long as ID implementation with Thread-Safe ID generator
 */
public class GraphLongFactory {

    // ID counter
    private static final AtomicLong idCounter = new AtomicLong(0);

    public static GraphLong buildSimpleGraph() {
        return new GraphBaseLong();
    }

    public static GraphLong buildThreadSafeGraph() {
        return new GraphLongSyncDecorator(buildSimpleGraph());
    }

    // Thread-Safe ID generator
    public static Long getNextId() {
        return idCounter.incrementAndGet();
    }

}
