package com.kit1414.graphlib.model.mlong;

import com.kit1414.graphlib.model.base.Graph;

/**
 * Graph interface With Long as ID
 */
public interface GraphLong extends Graph<VertexLong, EdgeLong, Long> {
}
