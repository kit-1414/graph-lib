# README #

test project to demonstrate java skills

## Graph Lib ##

project implements Graph library:

* Lib support 2 types of graphs - directed and undirected with operations:
    * addVertex - adds vertex to the graph    
    * addVertexes - adds vertex list to the graph
    * addEdge - adds edge to the graph   
    * addEdges - adds edge list to the graph
    * getPath - returns some path as a list of edges between 2 vertices (path is not optimal). Any Vertex happens once in path.    
    * getAllPath - returns all possible path as a list of edges between 2 vertices. Any Vertex happens once in path.        
    * traverseVertexes - apply user function to all Vertexes of Graph
    
* Features    
    * Edge has weighted.
    * Graph can be Thread Safe or not

Library developed as interface based.
Any user classes (Vertex and Eges) can be uses by library, if it implements base interfaces.

Main contract is:
* ID type class `<T>` must implements valid equals() && hashCode()
* All interfaces methods for Vertex && Eges must return const not null values after object created.    
## Implementation ###
Interfaces and base generic classes are in package **com.kit1414.graphlib.model.base**

## Usage ###
Simple implementation with ID "`<T>`  as Long" are in package **com.kit1414.graphlib.model.mlong**
    
## Test ###
There are unit tests for ID "`<T>` as Long" implementation in project.